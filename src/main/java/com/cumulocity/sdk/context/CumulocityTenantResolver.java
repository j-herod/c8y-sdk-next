package com.cumulocity.sdk.context;

import io.micronaut.multitenancy.exceptions.TenantNotFoundException;
import io.micronaut.multitenancy.tenantresolver.TenantResolver;

import java.io.Serializable;

public class CumulocityTenantResolver implements TenantResolver {

    @Override
    public Serializable resolveTenantIdentifier() throws TenantNotFoundException {
        return null;
    }

}
