package com.cumulocity.sdk.event;

import com.cumulocity.Application;
import io.micronaut.test.annotation.MicronautTest;
import io.reactivex.Maybe;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@MicronautTest(application = Application.class)
class EventApiTest {

    @Inject
    private EventApi eventApi;

    @Test
    void fetchEvents() {
        //given
        final Maybe<List<Event>> maybe = eventApi.fetchEvents();
        //when
        final List<Event> events = maybe.blockingGet();
        //then
        assertThat(events).hasSizeGreaterThan(0);
    }
}