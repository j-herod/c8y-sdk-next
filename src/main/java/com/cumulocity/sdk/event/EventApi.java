package com.cumulocity.sdk.event;

import com.cumulocity.sdk.PlatformConfiguration;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.uri.UriTemplate;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class EventApi {

    private final RxHttpClient httpClient;
    private final String uri;

    public EventApi(@Client(PlatformConfiguration.API_URL) RxHttpClient httpClient) {
        this.httpClient = httpClient;
        String path = "/event/events";
        uri = UriTemplate.of(path).toString();
    }

    public Maybe<List<Event>> fetchEvents() {
        HttpRequest<?> req = HttpRequest.GET(uri);
        Flowable flowable = httpClient.retrieve(req, Argument.of(List.class, Event.class));
        return (Maybe<List<Event>>) flowable.firstElement();
    }

}
