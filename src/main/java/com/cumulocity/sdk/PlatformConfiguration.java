package com.cumulocity.sdk;

import javax.inject.Singleton;

@Singleton
public class PlatformConfiguration {

    public static final String API_URL = "https://slawek.staging-latest.c8y.io";

    private String tenant = "slawek";

    private String username = "admin";

    private String password = "Pyi1bo1r";

    public String getTenant() {
        return tenant;
    }

    public void setTenant(final String tenant) {
        this.tenant = tenant;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

}