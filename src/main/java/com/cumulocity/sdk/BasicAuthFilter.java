package com.cumulocity.sdk;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.annotation.Filter;
import io.micronaut.http.filter.ClientFilterChain;
import io.micronaut.http.filter.HttpClientFilter;
import org.reactivestreams.Publisher;

@Filter
public class BasicAuthFilter implements HttpClientFilter {

    private final PlatformConfiguration configuration;

    public BasicAuthFilter(PlatformConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Publisher<? extends HttpResponse<?>> doFilter(MutableHttpRequest<?> request, ClientFilterChain chain) {
        return chain.proceed(request.basicAuth(configuration.getTenant() + "/" + configuration.getUsername(), configuration.getPassword()));
    }
}
