FROM oracle/graalvm-ce:19.2.0.1 as graalvm
COPY . /home/app/c8y-sdk-next
WORKDIR /home/app/c8y-sdk-next
RUN gu install native-image
RUN native-image --no-server -cp build/libs/c8y-sdk-next-*-all.jar

FROM frolvlad/alpine-glibc
EXPOSE 8080
COPY --from=graalvm /home/app/c8y-sdk-next .
ENTRYPOINT ["./c8y-sdk-next"]
