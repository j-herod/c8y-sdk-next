#!/bin/sh
docker build . -t c8y-sdk-next
echo
echo
echo "To run the docker container execute:"
echo "    $ docker run -p 8080:8080 c8y-sdk-next"
