package com.cumulocity.sdk.context;

import io.micronaut.context.BeanResolutionContext;
import io.micronaut.context.scope.CustomScope;
import io.micronaut.inject.BeanDefinition;
import io.micronaut.inject.BeanIdentifier;

import javax.inject.Provider;
import java.util.Optional;

public class TenantCustomScope implements CustomScope<TenantScope> {
    @Override
    public Class<TenantScope> annotationType() {
        return null;
    }

    @Override
    public <T> T get(final BeanResolutionContext resolutionContext, final BeanDefinition<T> beanDefinition, final BeanIdentifier identifier, final Provider<T> provider) {
        return null;
    }

    @Override
    public <T> Optional<T> remove(final BeanIdentifier identifier) {
        return Optional.empty();
    }
}
