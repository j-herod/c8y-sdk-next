package com.cumulocity.microservice;

import com.cumulocity.sdk.event.Event;
import com.cumulocity.sdk.event.EventApi;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import io.reactivex.Maybe;

import java.util.List;

@Controller
public class SimpleController {
    
    private final EventApi eventApi;

    public SimpleController(final EventApi eventApi) {
        this.eventApi = eventApi;
    }

    @Get("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello World";
    }

    @Get("/events")
    @Produces(MediaType.APPLICATION_JSON)
    public Maybe<List<Event>> events() {
        return eventApi.fetchEvents();
    }
    
}