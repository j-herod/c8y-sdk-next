package com.cumulocity.sdk.event;

import com.cumulocity.sdk.inventory.ManagedObject;

import java.util.Date;

public class Event {
    private String id;

    private String type;

    private Date time;

    private Date creationTime;

    private String text;

    private ManagedObject managedObject;
    
}
